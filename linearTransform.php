<?php
/**
 * Created by PhpStorm.
 * User: Moskalenko
 * Date: 11.10.2017
 * Time: 13:33
 */

class linearTransform
{

    private $matr0 = array(1, 1, 5, 1, 8, 6, 7, 4);
    private $matr1 = array(4, 1, 1, 5, 1, 8, 6, 7);
    private $matr2 = array(7, 4, 1, 1, 5, 1, 8, 6);
    private $matr3 = array(6, 7, 4, 1, 1, 5, 1, 8);
    private $matr4 = array(8, 6, 7, 4, 1, 1, 5, 1);
    private $matr5 = array(1, 8, 6, 7, 4, 1, 1, 5);
    private $matr6 = array(5, 1, 8, 6, 7, 4, 1, 1);
    private $matr7 = array(1, 5, 1, 8, 6, 7, 4, 1);

    private $matrix;

    private $splitMessage = array();
    private $splitMessageBin = array();
    private $splitMessageXs = array();

    private $multiply;
    private $matrixB;
    private $b;
    private $newMatrixB;
    private $bin;
    private $hex;

    private $polinom = array( 8, 4, 3, 2, 0);


    // Вызывает все функции в правильном порядке
    // Аргумент $msg - сообщение введенное пользователем
    // Возвращает результат преобразования - $hex
    public function calculate($msg) {

        $this->msgSplitting($msg);
        $this->msgToBin();
        $this->msgBinToX();
        $this->multiplyMatrix();
        $this->reductionB();
        $this->polinom();
        $this->x2bin();
        $this->bin2hex();

        return $this->hex;
    }

    private function bin2hex() {
        foreach ($this->bin as $value) {
            $this->hex .= base_convert($value, 2, 16) . ' ';
        }
    }

    //Конвертирует выражение с иксами в двоичную систему счисления
    private function x2bin() {
        for ($i = 0; $i < count($this->b); $i++) {
            $binBlank = '00000000';

            foreach ($this->b[$i] as $value) {
                $binBlank[strlen($binBlank) - $value - 1] = '1';
            }
            $this->bin[] = $binBlank;
        }
    }

    //  Выполняет проверку $newMatrixB[][] на наличие степеней выше 7 и записывает их в $temp[]. Если $temp[] не пустой
    //"делит" $newMatrixB[][] на полином (подбирает делитель в зависимости от значений в $temp[]) и сокращает повторяющиеся
    //иксы
    private function polinom(){

        //сортировка матрицы
        for ($i = 0; $i < count($this->newMatrixB); $i++) {
            rsort($this->newMatrixB[$i]);
        }

        for ($i = 0; $i < count($this->newMatrixB); $i++) {

            $tempPolinom = array();
            $temp = array();        //массив со степенями > 7

            //записываем в $temp степени которые больше 7 из newMatrixB
            for ($j = 0; $j < count($this->newMatrixB[$i]); $j++) {
                if ($this->newMatrixB[$i][$j] > 7)
                    $temp[] = $this->newMatrixB[$i][$j];
            }

            ////////////////////////////////////////////////////////////////////////////////////////////////////////////
            ///Подбор делителя
            ///
            if ($temp) {
                switch ($temp) {

                    case ($temp[0] == 10 && $temp[1] == false && $temp[2] == false):
                        for ($j = 0; $j < count($this->polinom); $j++) {

                            $tempPolinom[] = $this->polinom[$j] + 2;
                        }
                        break;

                    case ($temp[0] == 10 && $temp[1] == 9 && $temp[2] == 8):
                        for ($j = 0; $j < count($this->polinom); $j++) {

                            $tempPolinom[] = $this->polinom[$j] + 2;
                            $tempPolinom[] = $this->polinom[$j] + 1;
                            $tempPolinom[] = $this->polinom[$j];
                        }
                        break;

                    case ($temp[0] == 10 && $temp[1] == 9 && $temp[2] == false):
                        for ($j = 0; $j < count($this->polinom); $j++) {

                            $tempPolinom[] = $this->polinom[$j] + 2;
                            $tempPolinom[] = $this->polinom[$j] + 1;
                        }
                        break;

                    case ($temp[0] == 10 && $temp[1] == 8 && $temp[2] == false):
                        for ($j = 0; $j < count($this->polinom); $j++) {

                            $tempPolinom[] = $this->polinom[$j] + 2;
                            $tempPolinom[] = $this->polinom[$j];
                        }
                        break;

                    case ($temp[0] == 9 && $temp[1] == false && $temp[2] == false):
                        for ($j = 0; $j < count($this->polinom); $j++) {

                            $tempPolinom[] = ($this->polinom[$j] + 1);
                        }
                        break;

                    case ($temp[0] == 9 && $temp[1] == 8 && $temp[2] == false):
                        for ($j = 0; $j < count($this->polinom); $j++) {

                            $tempPolinom[] = $this->polinom[$j] + 1;
                            $tempPolinom[] = $this->polinom[$j];
                        }
                        break;

                    case ($temp[0] == 8 && $temp[1] == false && $temp[2] == false):
                        for ($j = 0; $j < count($this->polinom); $j++) {

                            $tempPolinom[] = $this->polinom[$j];
                        }
                        break;
                }
            }
            ///
            ///
            ////////////////////////////////////////////////////////////////////////////////////////////////////////////
            /// Сокращение повторяющихся иксов
            ///

            //добавление исходного выражения к полиному
            for ($j = 0; $j < count($this->newMatrixB[$i]); $j++) {
                $tempPolinom[] = $this->newMatrixB[$i][$j];
            }


            $countDegrees = array(); //массив [степень][количество повторений]

            //Задаем ключи массива со значением 0
            for ($j = 0; $j < count($tempPolinom); $j++) {
                $countDegrees[$tempPolinom[$j]] = 0;
            }

            //Считаем количество повторений
            for ($j = 0; $j < count($tempPolinom); $j++) {
                $countDegrees[$tempPolinom[$j]]++;
            }


            //Проверка четности количества степеней
            foreach ($countDegrees as $key => $value) {
                if ($value % 2 != 0)
                    $countDegrees[$key]--;
            }

            //Сокращение
            $newTempDegree = array();
            for ($j = 0; $j < count($tempPolinom); $j++) {
                if($countDegrees[$tempPolinom[$j]] == 0) {
                    $newTempDegree[] = $tempPolinom[$j];
                }
                else
                    $countDegrees[$tempPolinom[$j]]--;
            }


            rsort($newTempDegree);
            $this->b[] = $newTempDegree;

            unset($tempPolinom);
            unset($newTempDegree);
            unset($temp);
            unset($countDegrees);
        }
    }

    private function reductionB(){

        for($i = 0; $i < 8; $i++ ) {
            for($j = 0; $j < 8; $j++) {
                foreach ($this->multiply[$i][$j] as $values) {
                    $this->matrixB[$i][] = $values;
                }
            }
        }

        for ($i = 0; $i < 8; $i++) {
            $maxValue = 0;
            $tempNumb = array();
            foreach ($this->matrixB[$i] as $value) {
                if ($value > $maxValue)
                    $maxValue = $value;
            }

            for ($j = 0; $j < $maxValue; $j++) {
                $tempNumb[] = 0;
            }

            for($j = 0; $j < count($this->matrixB[$i]); $j++) {
                $tempNumb[$this->matrixB[$i][$j]]++;
            }


            for ($j = 0; $j < count($tempNumb); $j++) {
                if ($tempNumb[$j] % 2 != 0)
                    $tempNumb[$j]--;
            }

            for ($j = 0; $j < count($this->matrixB[$i]); $j++) {
                if($tempNumb[$this->matrixB[$i][$j]] == 0) {
                    $this->newMatrixB[$i][] = $this->matrixB[$i][$j];
                }
                else
                    $tempNumb[$this->matrixB[$i][$j]]--;
            }
        }

    }

    private function multiplyMatrix()
    {
        for ($i = 0; $i < 8; $i++) {

            for ($j = 0; $j < 8; $j++) {

                switch ($this->matrix[$i][$j]) {
                    case 1:
                        for ($c = 0; $c < count($this->splitMessageXs[($this->splitMessageBin[$j])]); $c++){
                            $this->multiply[$i][$j][] = $this->splitMessageXs[($this->splitMessageBin[$j])][$c];
                        }
                        break;

                    case 4:
                        for ($c = 0; $c < count($this->splitMessageXs[($this->splitMessageBin[$j])]); $c++){
                            $this->multiply[$i][$j][] = ($this->splitMessageXs[($this->splitMessageBin[$j])][$c] + 2);
                        }
                        break;

                    case 5:
                        for ($c = 0; $c < count($this->splitMessageXs[($this->splitMessageBin[$j])]); $c++){
                            $this->multiply[$i][$j][] = ($this->splitMessageXs[($this->splitMessageBin[$j])][$c] + 2);
                        }
                        for ($c = 0; $c < count($this->splitMessageXs[($this->splitMessageBin[$j])]); $c++){
                            $this->multiply[$i][$j][] = $this->splitMessageXs[($this->splitMessageBin[$j])][$c];
                        }
                        break;

                    case 6:
                        for ($c = 0; $c < count($this->splitMessageXs[($this->splitMessageBin[$j])]); $c++){
                            $this->multiply[$i][$j][] = ($this->splitMessageXs[($this->splitMessageBin[$j])][$c] + 2);
                        }
                        for ($c = 0; $c < count($this->splitMessageXs[($this->splitMessageBin[$j])]); $c++){
                            $this->multiply[$i][$j][] = ($this->splitMessageXs[($this->splitMessageBin[$j])][$c] + 1);
                        }
                        break;

                    case 7:
                        for ($c = 0; $c < count($this->splitMessageXs[($this->splitMessageBin[$j])]); $c++){
                            $this->multiply[$i][$j][] = ($this->splitMessageXs[($this->splitMessageBin[$j])][$c] + 2);
                        }
                        for ($c = 0; $c < count($this->splitMessageXs[($this->splitMessageBin[$j])]); $c++){
                            $this->multiply[$i][$j][] = ($this->splitMessageXs[($this->splitMessageBin[$j])][$c] + 1);
                        }
                        for ($c = 0; $c < count($this->splitMessageXs[($this->splitMessageBin[$j])]); $c++){
                            $this->multiply[$i][$j][] = $this->splitMessageXs[($this->splitMessageBin[$j])][$c];
                        }
                        break;

                    case 8:
                        for ($c = 0; $c < count($this->splitMessageXs[($this->splitMessageBin[$j])]); $c++){
                            $this->multiply[$i][$j][] = ($this->splitMessageXs[($this->splitMessageBin[$j])][$c] + 3);
                        }
                        break;

                }
            }
        }

    }

    private function msgSplitting($msg)
    {
        if (strlen($msg) != 16) {
            echo "Длинна сообщения не равна 16 символам";
            return;
        }

        for ($i = 0; $i < 16; $i++) {
            $this->splitMessage[] = $msg[$i] . $msg[($i+1)];
            $i++;
        }
    }

    private function msgToBin()
    {
        foreach ($this->splitMessage as $value) {
            $this->splitMessageBin[] = base_convert($value, 16, 2);
        }
    }

    private function msgBinToX()
    {


        for ($i = 0; $i < count($this->splitMessageBin); $i++) {

            $degree = 0;

            $this->splitMessageXs[$this->splitMessageBin[$i]];

            for ($j = (strlen($this->splitMessageBin[$i]) - 1); $j >= 0; $j--) {

                if ($this->splitMessageBin[$i][$j] !== '0') {

                    switch ($degree) {
                        case 0:
                            $this->splitMessageXs[$this->splitMessageBin[$i]][] = "0";
                            break;
                        case 1:
                            $this->splitMessageXs[$this->splitMessageBin[$i]][] = "1";
                            break;
                        default:
                            $this->splitMessageXs[$this->splitMessageBin[$i]][] = "$degree";
                    }
                }
                $degree++;
            }

        }



        foreach ($this->splitMessageBin as $value) {

            $degree = 0;

            $this->splitMessageXs[$value];

            for ($i = (strlen($value) - 1); $i >= 0; $i--) {

                if ($value[$i] !== '0') {

                    switch ($degree) {
                        case 0:
                            $this->splitMessageXs[$value][] = "0";
                            break;
                        case 1:
                            $this->splitMessageXs[$value][] = "1";
                            break;
                        default:
                            $this->splitMessageXs[$value][] = "$degree";
                    }
                }
                $degree++;
            }

            //  По непонятной причине, иногда цикл (или foreach или for, так и не понял) повторяется и дублируются
            // записи в splitMessageXs. Временно исправил проблему функцией array_unique().
            $this->splitMessageXs[$value] = array_unique($this->splitMessageXs[$value]);
        }


    }

    private function setMatrix()
    {
        $this->matrix = array($this->matr0, $this->matr1, $this->matr2, $this->matr3, $this->matr4, $this->matr5, $this->matr6, $this->matr7);
    }

    public function __construct()
    {
        $this->setMatrix();
    }
}